import sys

sys.path.append('src/')

from support import dice as roll
# import dice as roll


def get_alignment():
    x = roll.nd6(1)

    if x == 1 or x == 2:
        print('Lawful')
    elif x == 3 or x == 4:
        print('Neutral')
    elif x == 5 or x == 6:
        print('Chaotic')


# get_alignment()
