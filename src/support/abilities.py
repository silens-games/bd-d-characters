import sys
sys.path.append('src/')

from support import dice as roll


def fighter_abilities():
    while True:
        str = roll.nd6(3)
        int = roll.nd6(3)
        wis = roll.nd6(3)
        dex = roll.nd6(3)
        con = roll.nd6(3)
        cha = roll.nd6(3)

        abilities = [str, int, wis, dex, con, cha]

        max_ability = max(abilities)

        if str == max_ability:
            print('Fighter')
            print('Str = ', str)
            print('Dex = ', dex)
            print('Con = ', con)
            print('Int = ', int)
            print('Wis = ', wis)
            print('Cha = ', cha)

            break

def magic_user_abilities():
    while True:
        str = roll.nd6(3)
        int = roll.nd6(3)
        wis = roll.nd6(3)
        dex = roll.nd6(3)
        con = roll.nd6(3)
        cha = roll.nd6(3)

        # dtermine the max value between str, int, wis, dex, con, cha
        abilities = [str, int, wis, dex, con, cha]

        max_ability = max(abilities)

        if int == max_ability:
            print('Magic-user')
            print('Str = ', str)
            print('Dex = ', dex)
            print('Con = ', con)
            print('Int = ', int)
            print('Wis = ', wis)
            print('Cha = ', cha)

            break


def cleric_abilities():
    while True:
        str = roll.nd6(3)
        int = roll.nd6(3)
        wis = roll.nd6(3)
        dex = roll.nd6(3)
        con = roll.nd6(3)
        cha = roll.nd6(3)

        # dtermine the max value between str, int, wis, dex, con, cha
        abilities = [str, int, wis, dex, con, cha]

        max_ability = max(abilities)

        if wis == max_ability:
            print('Cleric')
            print('Str = ', str)
            print('Dex = ', dex)
            print('Con = ', con)
            print('Int = ', int)
            print('Wis = ', wis)
            print('Cha = ', cha)

            break

def thief_abilities():
    while True:
        str = roll.nd6(3)
        int = roll.nd6(3)
        wis = roll.nd6(3)
        dex = roll.nd6(3)
        con = roll.nd6(3)
        cha = roll.nd6(3)

        # dtermine the max value between str, int, wis, dex, con, cha
        abilities = [str, int, wis, dex, con, cha]

        max_ability = max(abilities)

        if dex == max_ability:
            print('Thief')
            print('Str = ', str)
            print('Dex = ', dex)
            print('Con = ', con)
            print('Int = ', int)
            print('Wis = ', wis)
            print('Cha = ', cha)

            break

def dwarf_abilities():
    while True:
        str = roll.nd6(3)
        int = roll.nd6(3)
        wis = roll.nd6(3)
        dex = roll.nd6(3)
        con = roll.nd6(3)
        cha = roll.nd6(3)

        # dtermine the max value between str, int, wis, dex, con, cha
        abilities = [str, int, wis, dex, con, cha]

        max_ability = max(abilities)

        if str == max_ability and con >= 9:
            print('Dwarf')
            print('Str = ', str)
            print('Dex = ', dex)
            print('Con = ', con)
            print('Int = ', int)
            print('Wis = ', wis)
            print('Cha = ', cha)

            break

def elf_abilities():
   while True:
       str = roll.nd6(3)
       int = roll.nd6(3)
       wis = roll.nd6(3)
       dex = roll.nd6(3)
       con = roll.nd6(3)
       cha = roll.nd6(3)

       # dtermine the max value between str, int, wis, dex, con, cha
       abilities = [str, int, wis, dex, con, cha]

       max_ability = max(abilities)

       abilities.remove(max_ability)

       max_ability = max(abilities)

       if str >= max_ability and int >= max_ability and int >= 9:
           print('Elf')
           print('Str = ', str)
           print('Dex = ', dex)
           print('Con = ', con)
           print('Int = ', int)
           print('Wis = ', wis)
           print('Cha = ', cha)

           break       


def hobbit_abilities():
   while True:
       str = roll.nd6(3)
       int = roll.nd6(3)
       wis = roll.nd6(3)
       dex = roll.nd6(3)
       con = roll.nd6(3)
       cha = roll.nd6(3)

       # dtermine the max value between str, int, wis, dex, con, cha
       abilities = [str, int, wis, dex, con, cha]

       max_ability = max(abilities)

       abilities.remove(max_ability)

       max_ability = max(abilities)

       if str >= max_ability and dex >= max_ability and dex >= 9 and con >= 9:
           print('Hobbit')
           print('Str = ', str)
           print('Dex = ', dex)
           print('Con = ', con)
           print('Int = ', int)
           print('Wis = ', wis)
           print('Cha = ', cha)

           break       


def druid_abilities():
    while True:
        str = roll.nd6(3)
        int = roll.nd6(3)
        wis = roll.nd6(3)
        dex = roll.nd6(3)
        con = roll.nd6(3)
        cha = roll.nd6(3)

        # dtermine the max value between str, int, wis, dex, con, cha
        abilities = [str, int, wis, dex, con, cha]

        max_ability = max(abilities)

        if wis == max_ability:
            print('Druid')
            print('Str = ', str)
            print('Dex = ', dex)
            print('Con = ', con)
            print('Int = ', int)
            print('Wis = ', wis)
            print('Cha = ', cha)

            break      


def mystic_abilities():
    while True:
        str = roll.nd6(3)
        int = roll.nd6(3)
        wis = roll.nd6(3)
        dex = roll.nd6(3)
        con = roll.nd6(3)
        cha = roll.nd6(3)

        abilities = [str, int, wis, dex, con, cha]

        max_ability = max(abilities)

        abilities.remove(max_ability)

        max_ability = max(abilities)

        if str >= max_ability and dex >= max_ability and wis >= 13 and dex >= 13:
            print('Mystic')
            print('Str = ', str)
            print('Dex = ', dex)
            print('Con = ', con)
            print('Int = ', int)
            print('Wis = ', wis)
            print('Cha = ', cha)

            break      

fighter_abilities()
magic_user_abilities()
cleric_abilities()
thief_abilities()
dwarf_abilities()
elf_abilities()
hobbit_abilities()
druid_abilities()
mystic_abilities()


