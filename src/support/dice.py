import random


def nd6(n):
    roll = 0

    for x in range(n):
        roll = roll + random.randint(1, 6)

    return roll


def nd8(n):
    roll = 0

    for x in range(n):
        roll = roll + random.randint(1, 8)

    return roll


def nd10(n):
    roll = 0

    for x in range(n):
        roll = roll + random.randint(1, 10)

    return roll


def nd12(n):
    roll = 0

    for x in range(n):
        roll = roll + random.randint(1, 12)

    return roll


def nd20(n):
    roll = 0

    for x in range(n):
        roll = roll + random.randint(1, 20)

    return roll


def nd100(n):
    roll = 0

    for x in range(n):
        roll = roll + random.randint(1, 100)

    return roll
