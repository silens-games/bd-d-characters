import sys
sys.path.append('src/')

from support import dice as roll


def ability_bonus(x):
    if x == 2 or x == 3:
        return -3
    if x == 4 or x == 5:
        return -2
    if x == 6 or x == 7 or x == 8:
        return -1
    if x == 9 or x == 10 or x == 11 or x == 12:
        return 0
    if x == 13 or x == 14 or x == 15:
        return 1
    if x == 16 or x == 17:
        return 2
    if x == 18:
        return 3


def hit_points_d6(ability):
    hp = roll.nd6(1)

    return hp + ability_bonus(ability)


def hit_points_d8(ability):
    hp = roll.nd8(1)

    return hp + ability_bonus(ability)


def hit_points_d10(ability):
    hp = roll.nd10(1)

    return hp + ability_bonus(ability)
