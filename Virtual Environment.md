# virtual environment

- ```python -m venv venv```

- ```.\venv\Scripts\activate```

- ```pip install <package-name>```

- ```deactivate``` 

## Create virtual environment:

```bash
python -m venv <name>  # or virtualenv venv
```
## Activate virtual environment:

### On Windows:
```bash
.\<name>\Scripts\activate
```

### On macOS/Linux:
```bash
source venv/bin/activate
```

## Install packages:

```bash
pip install <package-name>
```

## Deactivate virtual environment:

```bash
deactivate
```

