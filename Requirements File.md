# requirements.txt File

## Manually Create the requirements.txt File:

The content of the requirements.txt file would look like this:

```bash
Django==5.0.3
requests==2.31.0
```

## Install the Packages from requirements.txt:

Once you have the requirements.txt file, you can install the dependencies in your virtual environment by running:

```bash
pip install -r requirements.txt
 ```

This command will install the specified versions of Django and requests.

## Alternatively, 

if you're already in a virtual environment and have these packages installed, you can generate the requirements.txt file by running the following command:

```bash
pip freeze > requirements.txt
```

This will capture the exact versions of all installed packages, including `Django==5.0.3` and `requests==2.31.0`, and create a requirements.txt file. However, if you only want those specific packages, it's better to manually create the file as shown above.